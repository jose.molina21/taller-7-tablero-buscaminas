//clase
class Campo {
    misCeldas() {
        //variables
        let filas = 8;
        let columnas = 8;
        let tablero;
        let i;
        let j;
        let celdas;

        // Llamar el id de html
        tablero = document.getElementById("tablero");

        // Creacion de matriz 8x8
        for (i = 0; i < columnas; i++) {
            for (j = 0; j < filas; j++) {
                celdas = document.createElement("input");
                celdas.setAttribute("type", "button");
                celdas.setAttribute("id", "celda " + i + " " + j);
                celdas.setAttribute("onclick", "miTablero.ocultarCasilla('celda " + i + " " + j + "')");
                tablero.appendChild(celdas);
            }
        }
    }

    ocultarCasilla(id) {
        let casilla;
        casilla = document.getElementById(id);
        casilla.setAttribute("style", "background-color: white");
    }
}
//objeto
let miTablero = new Campo();
miTablero.misCeldas();